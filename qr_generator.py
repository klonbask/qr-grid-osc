import segno

with open("qr_codes.txt") as f:
    lines = f.readlines()
    
qr_codes = [line[:-1] for line in lines]

for code in qr_codes:
    qrcode = segno.make_qr(code)
    qrcode.save("qr_codes/" + code + ".png", scale=20)
