import cv2

# Define grid parameters
GRID_ROWS = 3
GRID_COLS = 4
EDGE_MARGIN = 50
LINE_WIDTH = 2
LEFT_MARGIN = 400

# Initialize video capture
cap = cv2.VideoCapture(0)

def draw_grid(frame):
    box_width = (frame.shape[1] - LEFT_MARGIN - 2 * EDGE_MARGIN) // GRID_COLS
    box_height = (frame.shape[0] - 2 * EDGE_MARGIN) // GRID_ROWS

    # Draw grid boxes
    for i in range(GRID_ROWS):
        for j in range(GRID_COLS):
            x = EDGE_MARGIN + j * (box_width) + LEFT_MARGIN
            y = EDGE_MARGIN + i * (box_height)
            cv2.rectangle(frame, (x, y), (x + box_width, y + box_height), (0, 255, 0), 2)

# Main loop to capture frames and draw grid
while True:
    ret, frame = cap.read()
    if not ret:
        break

    # Draw grid on the frame
    draw_grid(frame)

    # Display the frame
    cv2.imshow('Grid', frame)

    # Check for key press
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release video capture and close windows
cap.release()
cv2.destroyAllWindows()
