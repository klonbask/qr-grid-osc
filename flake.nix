{
  inputs = { 
    nixpkgs.url = "github:nixos/nixpkgs"; 
    flake-utils.url = "github:numtide/flake-utils";
    };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let 
          pkgs = nixpkgs.legacyPackages.x86_64-linux;
          python = let
            packageOverrides = self: super: {
               opencv4 = super.opencv4.override  { 
                enableGtk2 = true; # boolean
                gtk2 = pkgs.gtk2-x11;
                };
            };
          in 
            pkgs.python3.override {inherit packageOverrides; self = python;};
        in {
          devShells.default =
            pkgs.mkShell { 
              buildInputs = [ 
                (python.withPackages(ps: with ps; 
                  [ 
                    opencv4 
                    numpy 
                    python-osc 
                    pyzbar 
                    segno
                  ])) 
                ];
            };
       });
}
