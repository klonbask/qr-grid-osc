import cv2
import argparse
import numpy as np
from pyzbar.pyzbar import decode
from pythonosc import udp_client
from pythonosc import osc_message_builder

# Define grid parameters
GRID_ROWS = 3
GRID_COLS = 4
EDGE_MARGIN = 50
LINE_WIDTH = 2
LEFT_MARGIN = 400

# Define number of scans for OSC message
OSC_RATE = 3

# Define initial values
scancount = 0
qr_map = {'[0, 0]': ("blank", "blank", "blank"), '[0, 1]': ("blank", "blank", "blank"), '[0, 2]': ("blank", "blank", "blank"), '[1, 0]': ("blank", "blank", "blank"), '[1, 1]': ("blank", "blank", "blank"), '[1, 2]': ("blank", "blank", "blank"), '[2, 0]': ("blank", "blank", "blank"), '[2, 1]': ("blank", "blank", "blank"), '[2, 2]': ("blank", "blank", "blank"), '[3, 0]': ("blank", "blank", "blank"), '[3, 1]': ("blank", "blank", "blank"), '[3, 2]': ("blank", "blank", "blank"), }

def draw_grid(frame):
    # Calculate grid
    box_width = (frame.shape[1] - LEFT_MARGIN - 2 * EDGE_MARGIN) // GRID_COLS
    box_height = (frame.shape[0] - 2 * EDGE_MARGIN) // GRID_ROWS

    # Draw grid 
    for i in range(GRID_ROWS):
        for j in range(GRID_COLS):
            x = EDGE_MARGIN + j * (box_width) + LEFT_MARGIN
            y = EDGE_MARGIN + i * (box_height)
            cv2.rectangle(frame, (x, y), (x + box_width, y + box_height), (0, 255, 0), LINE_WIDTH)


if __name__ == "__main__":
    # Implement arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="127.0.0.1",
        help="The ip of the OSC server")
    parser.add_argument("--port", type=int, default=5005,
        help="The port the OSC server is listening on")
    parser.add_argument("--camera", type=int, default=0,
        help="The target camera")
    args = parser.parse_args()
    client = udp_client.SimpleUDPClient(args.ip, args.port)
    cap = cv2.VideoCapture(args.camera)

    while True:
        success, img = cap.read()
    
        if not success:
            continue    

        # Define grid relative to the frame size
        box_width = (img.shape[1] - LEFT_MARGIN - 2 * EDGE_MARGIN) // GRID_COLS
        box_height = (img.shape[0] - 2 * EDGE_MARGIN) // GRID_ROWS

        decoding = decode(img)
        
        # Empty dictionary variable
        MAP_RESET = {'[0, 0]': ("blank", "blank", "blank"), '[0, 1]': ("blank", "blank", "blank"), '[0, 2]': ("blank", "blank", "blank"), '[1, 0]': ("blank", "blank", "blank"), '[1, 1]': ("blank", "blank", "blank"), '[1, 2]': ("blank", "blank", "blank"), '[2, 0]': ("blank", "blank", "blank"), '[2, 1]': ("blank", "blank", "blank"), '[2, 2]': ("blank", "blank", "blank"), '[3, 0]': ("blank", "blank", "blank"), '[3, 1]': ("blank", "blank", "blank"), '[3, 2]': ("blank", "blank", "blank"), }

        # Check for QR codes
        if decoding:
            for code in decoding:
                # Get QR code
                decoded_data = code.data.decode("ISO-8859-1")
                #print(code.data)
                #print(decoded_data)

                # Draw outline of QR codes
                pts = np.array([code.polygon], np.int32)
                cv2.polylines(img, [pts], True, (255, 0, 0), 3)
                cv2.rectangle(img, (code.rect[0], code.rect[1]), (code.rect[0] + code.rect[2], code.rect[1] + code.rect[3]), (0, 0, 255), 3)

                # Get and draw mid point of QR codes
                midpoint = (code.rect[0] + code.rect[2] // 2, code.rect[1] + code.rect[3] // 2)
                cv2.circle(img, midpoint, 3, (0, 0, 255), 10)
                
                # Find position in grid
                grid_x = (midpoint[0] - LEFT_MARGIN - EDGE_MARGIN) // box_width
                grid_y = (midpoint[1] - EDGE_MARGIN) // box_height
                position_xy = str([grid_x, grid_y])
                scancount += 1
                if decoded_data:
                    qr_map[position_xy] = [decoded_data, code.orientation]

        else:
            scancount += 1
            qr_map = MAP_RESET
               
        if (scancount >= OSC_RATE):
            # Convert dictionary to a string and remove curly braces
            qr_map_to_osc = str(qr_map)[1:-1]
            #print(qr_map_to_osc)

            client.send_message("/QR", qr_map_to_osc)
            #print(qr_map_to_osc)
            scancount = 0
            qr_map = MAP_RESET

        # Draw grid on the frame
        draw_grid(img)

        # Stop program
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        cv2.imshow("image", img)
        cv2.waitKey(1)

    cap.release()

