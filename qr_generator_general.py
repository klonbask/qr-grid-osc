import segno
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--txt",
        help="The txt file to read")
    parser.add_argument("--folder",
        help="The destination folder")
    parser.add_argument("--scale", type=int, default=1,
        help="Scaling of the QR code")
    parser.add_argument("--border", type=int, default=0,
        help="Border sorrounding the QR code")
    args = parser.parse_args()
    
    with open(args.txt) as f:
        lines = f.readlines()
        
    qr_codes = [line[:-1] for line in lines]

    for code in qr_codes:
        qrcode = segno.make_qr(code)
        qrcode.save(args.folder + "/" + code + ".png", scale=args.scale, border=args.border)
        print("Generated", args.folder + "/" + code + ".png")
